#!/bin/bash 


name="devsystem"
gitolite_port="2211"
jenkins_port="8088"

compose_in="docker-compose.yml.in"
compose="docker-compose.yml"

while getopts ":hn:g:j:i:o:" opt; do
	case ${opt} in
		h )
			echo "derp"
			exit 0
			;;
		n )
			name=$OPTARG
			;;
		g )
			gitolite_port=$OPTARG
			;;
		j )
			jenkins_port=$OPTARG
			;;
		i )
			compose_in=$OPTARG
			;;
		o )
			compose=$OPTARG
			;;
		\? )
			echo "DERP!!"
			exit -1
			;;
		: )
			echo "HEPR!!"
			exit -1
			;;
	esac
done

export name gitolite_port jenkins_port

function volume-exists {
	listing=$(docker volume ls | grep "$1")

	[ -n "$listing" ]
}

function volume {
	listing=$(docker volume ls | grep "$1")


	if volume-exists "$1"
	then
		echo "volume $1 exists"
	else
		echo "creating volume $1"
		docker volume create $1
	fi
}

function render_template {
	eval "echo \"$(cat $1)\""
}

#echo ${name}_gitolite-git

render_template ${compose_in} > ${compose}


if ! volume-exists ${name}_gitolite-sshkeys
then
	echo "Creating gitolite-sshkeys volume..."
	docker volume create --name ${name}_gitolite-sshkeys
	docker run --rm \
		-e SSH_KEY_NAME="$(whoami)" \
		-e SSH_KEY="$(cat ~/.ssh/id_rsa.pub)" \
		-v ${name}_gitolite-sshkeys:/etc/ssh/keys \
		-v ${name}_gitolite-git:/var/lib/git \
		jgiannuzzi/gitolite true
else
	echo "gitolite-sshkeys volume exists."
fi

#docker run --rm --name gitolite -p 2211:22 -v gitolite-sshkeys:/etc/ssh/keys -v gitolite-data:/var/lib/git jgiannuzzi/gitolite

#volume jenkins-data

#docker run --rm --name jenkins -p 8088:8080 --link=gitolite -v jenkins-data:/var/jenkins_home jenkinsci/blueocean

docker-compose -p ${name} up -d

# wait for it to come up
sleep 10 

# Make sure we know the key for the new host so we can automatically connect to it.
ssh-keyscan -t rsa -p ${gitolite_port} localhost >> ~/.ssh/known_hosts
ssh-keygen -Hf ~/.ssh/known_hosts

# Create dev-system on the new gitolite
git clone ssh://git@localhost:${gitolite_port}/gitolite-admin
cd gitolite-admin/conf

cat >>gitolite.conf <<EOF
repo  dev-system
    RW+    =    $(whoami)
EOF

cd ..
git add .
git commit -m "Create dev-system project"
git push

cd ..

# Switch origin to new and push
git remote set-url origin ssh://git@localhost:${gitolite_port}/dev-system
git push

# Create 'local-dev' branch and switch to it.

# Output Jenkins initialization key


#TODO:
# * Create jenkins user in gitolite
# * Create credential in jenkins
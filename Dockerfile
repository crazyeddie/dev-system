FROM ubuntu

RUN apt-get update 
RUN apt-get install -y python3-sphinx
RUN apt-get install -y python3-sphinx 
RUN apt-get install -y graphviz 
RUN apt-get install -y python3-pip 
RUN pip3 install sphinxcontrib-plantuml
RUN pip3 install breathe

VOLUME /documents

RUN chown -R 1000:1000 /documents

USER 1000:1000
WORKDIR /documents



